# Integração para Ecommerce


#### **Cenário 1: Guia de Integração Simples com Ecommerce**

> Vamos supor que você deseje fazer a integração para um ecommerce. 
> Por onde começar?

Os objetivos da sua integração podem ser:

- Poder registrar um Cliente no ERP a partir da plataforma de ecommerce
- Poder registrar um Pedido no ERP a partir da plataforma de ecommerce
- Exibir o Estoque atualizado dos Produtos
- Receber as alterações dos Produtos alterados

**Quais as features recomendadas?**

Feature                          | Descrição
---------------------------------|----------
TOKEN DE AUTENTICAÇÃO (opcional) | Link
CADASTRO E BUSCA DE CLIENTES     | Link
BUSCA DE PRODUTOS                | Link
CADASTRO DE PEDIDOS              | Link

---







