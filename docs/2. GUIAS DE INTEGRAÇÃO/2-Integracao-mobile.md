# Integração para Mobile


#### **Cenário 1: Guia de Integração Simples com Mobile**

> Vamos supor que você deseje fazer a integração para um aplicativo mobile. 
> Por onde começar?

Os objetivos da sua integração podem ser:

- Poder fazer o login de um usuário(vendedor)
- Poder registrar um Cliente no ERP a partir do aplicativo
- Receber as alterações dos Produtos alterados
- Exibir o Estoque atualizado dos Produtos
- Poder registrar um Pedido no ERP a partir do aplicativo

**Quais as features recomendadas?**

Feature                          | Descrição
---------------------------------|----------
TOKEN DE AUTENTICAÇÃO (opcional) | [Link](3-resumo-features.md#feature-obtendo-token-de-autenticação)
CADASTRO E BUSCA DE CLIENTES     | Link
BUSCA DE PRODUTOS                | Link
BUSCA DE ESTOQUE                 | Link
CADASTRO DE PEDIDOS              | Link
INFORMAÇÕES DO USUÁRIO           | Link

---