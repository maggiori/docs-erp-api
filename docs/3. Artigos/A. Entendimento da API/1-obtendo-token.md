
# Autenticação no sistema


> Para identificar o usuário utilizando nossa API, utilizamos um token de autenticação. 

### O que é o token de autenticação?

É um token que identifica um usuário do sistema, e que realiza o processo de autenticação e autorização do usuário. Sem ele, não é possível acessar a API. Este token deve ser enviado no `header` de todas as requisições.

Token: `netone-auth-token`

----

### Como obter o  token de autenticação?

Você pode obter token de autenticação por duas formas.

----

#### 1. Via ERP

Cada usuário possui um token gerado, que fica armazenado em seu cadastro no ERP. Se sua integração não tem a necessidade de autenticar diferentes usuários, você poderia obter o token via ERP e armazená-lo em sua aplicação/servidor.

> Este é um padrão mais comuns em Integrações via Ecommerce, onde existe uma única aplicação com acesso à API. Para Integrações que exigem múltiplos usuários, recomendamos a próxima forma.

Passos:
- Informe Usuário e a Senha 
- Acesse o Cadastro do seu usuário
- Editar / Integração
- Copie o Token que será exibido na tela
- Salvar

![Tela Login](../../../assets/images/tela-login.png)

![Tela Usuarios](../../../assets/images/usuario-editar.png)

![Tela Integração](../../../assets/images/usuario-integracao.png)

![Tela Integração](../../../assets/images/usuario-integracao-2.png)

Pronto! Agora é só usar este token na requisição via API

----



#### 2. Obtenção do Token via API

É possível obter o token via API, através de suas credenciais de usuário. Basta passar para o serviço `/auth/login` um usuário e senha cadastrados no sistema, que a API retornará o respectivo token do usuário.

> Só lembrando que, o usuário precisa estar previamente habilitado para usar a integração via API. Para mais informações, consulte o suporte.


----

### O que eu faço se perder o token?

Neste caso, você deve gerar o token novamente. Desta forma, o token antigo será invalidado e não poderá mais ser usado.


















