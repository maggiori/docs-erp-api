# Modelos de Response da API

Para facilitar a integração com a API, muitos dos `Responses` possuem uma estrutura similar.

#### `Response de Erro`

Quando ocorre algum erro no processamento, a API retorna um `HTTP Code` diferente de 200. Geralmente 400 ou 500. Quando o `HTTP Code` é de erro, sempre retornamos um objeto padrão de Erro: `ErroResponse`. Veja o modelo em [ErrorResponse](../../../models/ResponseError.v1.yaml).


#### `Response de Consulta`
Quando o serviço é para consultar dados, temos uma estrutura familiar: um objeto com campo `total`, `ultimaAlteracaoTimestamp`, e um array `result`. A maioria de nossos serviços de consulta utiliza este padrão. 
