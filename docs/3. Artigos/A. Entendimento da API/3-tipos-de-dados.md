# Tipos de Dados

Tipos de Dados comuns na API


### `Data - Timestamp`

- Formato esperado:
  - Date and Time - ISO-8601
  - Formato: `yyyy-MM-dd'T'HH:mm:ss.SSSZ`.
- Explicando o pattern:
  - yyyy: Anos
  - MM: mês (01-12)
  - dd: dia do mês (01-31)
  - T: é o delimitador para Hora
  - HH: Hora do dia (00-23)
  - mm: Minuto da hora (00-59)
  - ss: Segundo da hora (00-59)
  - SSS: fração decimal do segundo
  - Z: Timezone


> **O que é ISO-8601?**
>
> É uma norma para representação de data e hora pelo ISO.




Exemplo:
```
{
	"ultimaAlteracaoTimestamp": "2021-03-10T08:46:00.971-03:00"
}
```

### `Data - Date`

- Formato: `yyyy-MM-dd`
- Dica: Quando o campo é apenas de Data, o nome do campo geralmente termina com "date"

Exemplo:
```
{
	"emissaoDate": "2021-03-10"
}
```
