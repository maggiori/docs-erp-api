# Endpoints da API


### Para as Bases de Produção

- Não existe um caminho único para a Produção.   
- Cada empresa na Base de Produção possui seu subdomínio próprio. 
- Para saber mais, por favor, entre em contato com o suporte.

---

### Bases de Teste

Você pode fazer os testes de homologação usando o servidor abaixo:

Cluster | Base URI 
--------|----------
`Dev` | http://201.86.95.126:8082/app-web/api/v2/ 


> `ATENÇÃO:` Por serem bases de teste, elas estão em constante atualização. Não garantimos a disponibilidade do serviço o tempo todo. Além disso, os dados armazenados nestas bases também podem sofrer alterações sem aviso prévio.

> Para saber qual é o cluster utilizado para sua empresa, entre em contato com o suporte.

---
