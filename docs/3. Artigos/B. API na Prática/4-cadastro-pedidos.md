# Cadastro de Pedidos

> Este serviço permite cadastrar um Pedido no ERP.

----

### Cadastro Básico
Para cadastrar um Pedido Básico, você precisa informar pelo menos os seguintes itens.


**Capa do Pedido**

- `numeroOrigem` É o número de origem do pedido
- `emissaoDate` Data de Emissão do Pedido
- `previsaoEntregaDate` Previsão de Entrega do Pedido. Deve ser igual ou posterior à data de Emissão.
- `cliente` Informe o CPF/CNPJ se o cliente já existe. Informe todos os dados caso seja um cliente novo.
- `itens`: Itens do Pedido

**Item do Pedido**
- `idReduzido` Identificador do reduzido. Você obtem ele na listagem de [reduzidos](2-busca-reduzidos.md)
- `quantidade` Quantidade do item
- `valor` Valor unitário

------

### Informações Adicionais

`observacaoInterna`

É a observação que deve ficar disponível apenas internamente, para a empresa.

`observacaoExterna`

É a observação que será impressa na cópia do pedido


`valorFrete`

É possível informar o valor de frete


`formaCobrancaId`

Identificador da Forma de Cobrança.
Por padrão, a Forma de Cobrança usada é a informada no **Parâmetro do Faturamento**. Use este campo p/ informar uma Forma de Cobrança específica.

`condicaoPagamentoId`

Identificador da Condição de Pagamento.
Por padrão, a Condição de Pagamento usada é a informada no **Parâmetro do Faturamento**. Use este campo p/ informar uma Condição de Pgto específica.



----



















































