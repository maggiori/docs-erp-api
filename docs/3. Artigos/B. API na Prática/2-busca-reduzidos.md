
# Implemente a Busca dos Reduzidos

`Dicas p/ implementar:`
- Use o serviço `/produto/reduzidos`
- Armazene os dados dos reduzidos em algum storage
- Sincronize os dados periodicamente

----

#### Sincronizando os dados periodicamente

Para poder obter as últimas atualizações de produtos, evite buscar todos os produtos.
Use o param `ultimaDataAlteracaoTimestamp` no request da api para trazer apenas os produtos atualizados desde a data informada.

**Estratégias de sincronização:**

**Opção 1: Armazenando data da alteração: **

- No response da api, enviamos a `ultimaDataAlteracaoTimestamp` dos dados pesquisados. 
- Você pode salvar este valor toda vez que sincronizar, e na próxima requisição, passar esta data no request.

**Opção 2: Sempre buscar alterações até 24h atrás:**
- Uma outra opção é não salvar a data. 
- Ao invés disso, você sempre busca as alterações de 24h atrás.

----





