# Testando API via Postman

Para facilitar os testes da API, disponibilizamos uma Collection do Postman, contendo todos os endpoints da API.

Collection Postman:
- Link:  https://bitbucket.org/maggiori/docs-erp-api/raw/master/assets/postman/ERP_API_V2_1.postman_collection.json
- Ultima Atualização: 22/01/25

----

#### Importando o arquivo

Importe a collection no seu Postman


![Importe Collection](../../../assets/images/postman-import.png)

----


#### Configurando ambiente

Em todos os serviços declarados no postman, nós usamos variáveis para definir a URL completa do serviço.

As variáveis são:
- `host`: Host do servidor (ex: https://teste.net1ti.com.br)
- `port`: Port do servidor (ex: 443)

Para poder usar, você precisa criar um novo environment e setar os valores corretamente.


![Configure Environment](../../../assets/images/postman-new-environment.png)


----

#### Usando credenciais do usuário

Neste arquivo, nós automatizamos a autenticação do usuário. Para evitar ter que inserir manualmente o `header` de autenticação da API em cada serviço, nós armazenamos ele numa `global variable`.

A variável global é a `netone-auth-token`. Ele contém o token que é passado em cada endpoint da API.

Você pode informar manualmente o token nesta variável, na tela **"Manage Environments > Globals"**
![Global Var](../../../assets/images/postman-global-var.png)


Ou então, você pode fazer a autenticação do usuário pela própria API.

Encontre o serviço `/api/v2/auth/login`.

![Login via API](../../../assets/images/postman-login.png)

Envie uma requisição, passando o usuario e senha correta. Na resposta da requisição, existe um script (na Aba Tests) que seta automaticamente a variável global `netone-auth-token`.

Uma vez feito o login, você pode executar qualquer outro serviço, que o postman incluirá o token automaticamente para você.

































