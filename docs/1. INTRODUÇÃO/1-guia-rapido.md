# Guia Rápido

## Bem vindo à API v2

> Esta documentação vai apresentar um passo a passo para criar uma integração de maneira rápida e objetiva. Os serviços disponíveis em nossa API estão exibidos no menu a esquerda, sendo possível, ao clicá-los, a visualização de exemplos de uso assim como a descrição de cada parâmetro utilizado.

> Oferecemos a partir da documentação a opção de teste dos serviços, para essa operação, clique no botão 'Try it out' do serviço e certifique-se de utilizar seu token netone-auth-token de teste.

----



